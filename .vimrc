set binary
set nu
syntax enable
set autoindent
set ts=2	
set background=dark
colorscheme solarized
ino jj <esc>
cno jj <c-c>
let g:ackprg="ack -H --nocolor --nogroup --column"
if has("gui_running")
set guioptions-=T
set guifont=Menlo\ Regular:h14
endif
